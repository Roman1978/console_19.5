﻿#include <iostream>

using namespace std;

class Animal
{
public:
    virtual void Voice()
    {
        cout << "Animal" "\n";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        cout << "Woof" "\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "mew" "\n";
    }
};

class Pig : public Animal
{
public:
    void Voice() override
    {
        cout << "Oink" "\n";
    }
};

int main()
{
    Animal* p1 = new Dog();
    Animal* p2 = new Cat();
    Animal* p3 = new Pig();

    p1->Voice();
    p2->Voice();
    p3->Voice();
}

